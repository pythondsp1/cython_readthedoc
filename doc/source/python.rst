Upload Python Autodoc
=====================

Python file is saved as test.py in digcom folder. Location of python module must be included in conf.py. See conf.py for more details.

.. module:: test

.. autoclass:: math2
    :members:

