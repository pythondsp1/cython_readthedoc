Upload Cython and Python Documents on ReadTheDoc
================================================

Please `clone the repository <https://bitbucket.org/pythondsp/cython_readthedoc>`_  using following command to understand this document. Next, change the settings of the ReadTheDoc according to this tutorial to upload cython and python documents on ReadTheDoc along with autodoc features. 

.. code-block:: shell

  $ git clone https://pythondsp@bitbucket.org/pythondsp/cython_readthedoc.git

.

ReadTheDoc Settings
-------------------

1. Keep **setup.py in main directory**, as ReadTheDoc checks for it there only. 
2. Keep requirements.txt file in main directory
3. In conf.py insert the path for python files and cython files (read comments in conf.py for more details). Also, comment 'version' and 'release' lines in conf.py to prevent the display version number and realease number on the document.
4. On ReadTheDoc site, enable virtual env, fill the location of requirements.txt (simply write requirements.txt in the box) and select python version e.g. CPython3.x or CPython2.x.
5. Run python **setup.py build_ext --inplace** for local-html generation. ReadTheDoc will run this command by itself.



.. code-block:: unix 

  python setup.py build_ext --inplace


Documentation of digcom.pyx
---------------------------

This doucment is saved in **digcom** folder.

Note that only two classes are added in digcom.pyx i.e. SignalGenerator and TheoryBER. 

Since TheoryBER is not included in autosummary, therefore is not displayed in the table, but it's content is shown below the table due to automodue-digcom.

.. note::

  **Click on classes in the below table to see further details.**

.. automodule:: digcom
   
   .. rubric:: List of Classes

   .. autosummary::
        :toctree:: generated
   
        DecisionSignal
        ErrorCalcualtion
        SignalGenerator
      
   .. rubric:: Description for Classes and Methods 
   


..


Documentation of meher.pyx
--------------------------

This document is saved in **meher** folder.

.. note::

  **Click on classes in the below table to see further details.**

.. automodule:: meher
   
   .. rubric:: List of Classes

   .. autosummary::
        :toctree:: generated
   
        MeherDecisionSignal
        MeherErrorCalcualtion
        MeherSignalGenerator
        MeherTheoryBER
      
   .. rubric:: Description for Classes and Methods 
   
